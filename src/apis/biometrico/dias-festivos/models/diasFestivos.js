/* eslint-disable prettier/prettier */
const moongose = require('mongoose')

var ObjectId = moongose.Schema.Types.ObjectId
var diasFestivosSchema = moongose.Schema(
  {
    calendario: [
      {
        _id: ObjectId,
        fecha: String,
        descripcion: String,
      },
    ],
    anio: String,
  },
  {
    versionKey: false,
  },
)

diasFestivosSchema.set('collection', 'dias_festivos')

const diasFestivosModel = moongose.model('dias_festivos', diasFestivosSchema)

module.exports = diasFestivosModel