/* eslint-disable prettier/prettier */
require("dotenv").config();
const { mongoose } = require("mongoose");
const diasFestivosModel = require("./models/diasFestivos");

/**
 * @swagger
 * components:
 *  schemas:
 *    DiasFestivos:
 *      type: object
 *      properties:
 *        calendario:
 *          type: array
 *          items:
 *            type: object
 *            properties:
 *              _id:
 *                type: string
 *                description: Unique id of the holiday
 *              fecha:
 *                type: string
 *                description: Date of the holiday
 *              descripcion:
 *                type: string
 *                description: Description of the holiday
 *        anio:
 *          type: string
 *          description: Year of the holidays
 *      required:
 *        - calendario
 *        - anio
 *      example:
 *        calendario:
 *          - _id: 5e9f8f8f9f8f8f8f8f8f8f8
 *            fecha: "2020-01-01"
 *            descripcion: "Dia festivo"
 *        anio: "2020"
 */

/**
 * @swagger
 * /biometrico/dias-festivos:
 *  get:
 *    summary: Get all holidays
 *    tags: [DiasFestivos]
 *    responses:
 *      200:
 *        description: An array of holidays
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              items:
 *                $ref: '#/components/schemas/DiasFestivos'
 *      204:
 *        description: No holidays found
 *      500:
 *        description: Internal server error
 */

const obtenerDiasFestivos = async (req, res) => {
  await diasFestivosModel
    .find()
    .then((diasFestivos) => {
      if (diasFestivos?.length > 0) {
        return res.status(200).json({
          message: "Vacaciones encontradas",
          diasFestivos,
        });
      } else {
        return res.status(204).json({
          message: "Vacaciones no encontradas",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({
        message: "Error en el servidor interno",
      });
    });
};

/**
 * @swagger
 * /biometrico/dias-festivos/{anio}:
 *  get:
 *    summary: Get holidays by year
 *    tags: [DiasFestivos]
 *    parameters:
 *      - in: path
 *        name: anio
 *        schema:
 *          type: string
 *          description: Year of the holidays
 *        required: true
 *    responses:
 *      200:
 *        description: An array of holidays
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              items:
 *                $ref: '#/components/schemas/DiasFestivos'
 *      204:
 *        description: No holidays found
 *      500:
 *        description: Internal server error
 */

const obtenerDiasFestivosPorAnio = async (req, res) => {
  const { anio } = req.params;
  await diasFestivosModel
    .findOne({ anio })
    .then((diasFestivos) => {
      if (diasFestivos) {
        return res.status(200).json(diasFestivos);
      } else {
        return res.status(204).json({
          message: "No se encontaron días festivos",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({
        message: "Error en el servidor interno",
      });
    });
};

/**
 * @swagger
 * /biometrico/dias-festivos:
 *  post:
 *    summary: Create a new holiday
 *    tags: [DiasFestivos]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/DiasFestivos'
 *    responses:
 *      201:
 *        description: Holiday created
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/DiasFestivos'
 *      500:
 *        description: Internal server error
 */

const crearDiasFestivos = async (req, res) => {
  const { fecha, descripcion, anio } = req.body;
  return await diasFestivosModel
    .findOne({ anio })
    .then((diasFestivos) => {
      console.log(diasFestivos);
      if (diasFestivos) {
        diasFestivos.calendario.push({
          _id: new mongoose.Types.ObjectId(),
          fecha,
          descripcion,
        });
        diasFestivos.save().then((diasFestivos) => {
          return res.status(201).json(diasFestivos);
        });
      } else {
        const diasFestivosNuevo = new diasFestivosModel({
          calendario: [
            {
              _id: new mongoose.Types.ObjectId(),
              fecha,
              descripcion,
            },
          ],
          anio,
        });
        diasFestivosNuevo.save().then((diasFestivos) => {
          return res.status(201).json(diasFestivos);
        });
      }
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({
        message: "Error en el servidor interno",
      });
    });
};

/**
 * @swagger
 * /biometrico/dias-festivos/{id}:
 *  put:
 *    summary: Update holidays by year
 *    tags: [DiasFestivos]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *          description: Unique id of the holiday
 *        required: true
 *        description: Unique id of the holiday
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/DiasFestivos'
 *    responses:
 *      201:
 *        description: Holiday updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/DiasFestivos'
 *      500:
 *        description: Internal server error
 */

const actualizarDiasFestivos = async (req, res) => {
  const { id } = req.params;
  const { nueva_fecha, anio, descripcion } = req.body;
  return await diasFestivosModel
    .findOne({ anio })
    .then((diasFestivos) => {
      if (diasFestivos) {
        let found = false;
        diasFestivos.calendario.forEach((dia) => {
          if (dia._id.toString() === id) {
            dia.fecha = nueva_fecha;
            dia.descripcion = descripcion;
            found = true;
          }
        });
        if (found) {
          diasFestivos.save().then((diasFestivos) => {
            return res.status(201).json(diasFestivos);
          });
        } else {
          return res.status(404).json({
            message: "No se encontraron vacaciones",
          });
        }
      } else {
        return res.status(404).json({
          message: "No se encontraron vacaciones",
        });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

/**
 * @swagger
 * /biometrico/dias-festivos/{id}:
 *  delete:
 *    summary: Delete holidays by year
 *    tags: [DiasFestivos]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: Unique id of the holiday
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              anio:
 *                type: string
 *    responses:
 *      200:
 *        description: Holiday deleted
 *      500:
 *        description: Internal server error
 */

const eliminarDiasFestivos = async (req, res) => {
  const { id } = req.params;
  const { anio } = req.body;
  await diasFestivosModel
    .findOne({ anio })
    .then((diasFestivos) => {
      if (diasFestivos) {
        const index = diasFestivos.calendario.findIndex(
          (diaFestivo) => diaFestivo._id.toString() === id
        );
        if (index !== -1) {
          diasFestivos.calendario.splice(index, 1);
          diasFestivos.save().then(() => {
            return res.status(200).json({
              message: "Vacaciones detectadas",
            });
          });
        } else {
          return res.status(404).json({
            message: "No se encontraron vacaciones",
          });
        }
      } else {
        return res.status(404).json({
          message: "No se encontraron vacaciones",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({
        message: "Error en el servidor interno",
      });
    });
};

module.exports = {
  obtenerDiasFestivos,
  obtenerDiasFestivosPorAnio,
  crearDiasFestivos,
  actualizarDiasFestivos,
  eliminarDiasFestivos,
};
