/* eslint-disable prettier/prettier */
const moongose = require('mongoose')

var ObjectId = moongose.Schema.Types.ObjectId
var biometricoSchema = moongose.Schema(
  {
    apellidos: String,
    cod_reloj: String,
    nombres: String,
    numero_identificacion: String,
    uid: ObjectId,
    marcaciones: [
      {
        _id: ObjectId,
        fecha: String,
        horas: [
          {
            _id: ObjectId,
            entrada: String,
            salida: String,
          },
        ],
      },
    ],
  },
  {
    versionKey: false,
  },
)

biometricoSchema.set('collection', 'biometrico')

const biometricoModel = moongose.model('biometrico', biometricoSchema)

module.exports = biometricoModel
