/* eslint-disable prettier/prettier */
const { Router } = require('express')
const { obtenerMarcacionesPorAnio } = require('./controller')
const { obtenerDiasFestivos,
    obtenerDiasFestivosPorAnio,
    crearDiasFestivos,
    actualizarDiasFestivos,
    eliminarDiasFestivos,
} = require('./dias-festivos/controller')
const { obtenerTodasSolicitudesBiometrico, solicitarCambioBiometrico, solicitarRegistroBiometrico, validarSolicitudBiometrico, obtenerSolicitudesPorIndetificacion, eliminarSolititud } = require('./solicitudes/controller')


const biometrico = Router()


// -- Solicitudes -- //
biometrico.get('/biometrico/solicitudes/', obtenerTodasSolicitudesBiometrico)
biometrico.get(
    '/biometrico/solicitudes/:numero_identificacion', obtenerSolicitudesPorIndetificacion,
)
biometrico.post('/biometrico/solicitudes-cambio/', solicitarCambioBiometrico)
biometrico.post('/biometrico/agregar-solicitud/', solicitarRegistroBiometrico)
biometrico.put('/biometrico/solicitudes/validar/:id', validarSolicitudBiometrico)
biometrico.delete('/biometrico/eliminar-solicitudes/:id', eliminarSolititud)


// -- Dias Festivos 🌅 -- //
biometrico.get('/biometrico/dias-festivos/', obtenerDiasFestivos)
biometrico.get('/biometrico/dias-festivos/:anio', obtenerDiasFestivosPorAnio)
biometrico.post('/biometrico/dias-festivos/', crearDiasFestivos)
biometrico.put('/biometrico/dias-festivos/:id', actualizarDiasFestivos)
biometrico.delete('/biometrico/dias-festivos/:id', eliminarDiasFestivos)

module.exports = biometrico