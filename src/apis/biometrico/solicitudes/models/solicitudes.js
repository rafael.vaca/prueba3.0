const moongose = require("mongoose")

const ObjectId = moongose.Types.ObjectId
var solicitudesShema = moongose.Schema(
    {
        numero_indentificacion: String,
        nombre_Solicitante: String,
        fecha_identificacion: ObjectId,
        fecha_solicitud: String,
        hora_identificacion: ObjectId,
        hora_entrada: String,
        hora_salida: String,
        estado: Boolean,
        observacion: String,
        motivo: String,
        fecha_aprobacion: String,
        departamento_identificacion: String,
        nuevo_registro: Boolean,
    },
    {
        versionKey: false,
    },
)

solicitudesShema.set('collection', 'solicitud_biometrico')

const solicitudesModel = moongose.model('solicitudes_biometrico', solicitudesShema)

module.exports = solicitudesModel