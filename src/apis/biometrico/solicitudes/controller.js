require('dotenv').config()

const mongoose = require('mongoose')

const BiometricoModel = require('../models/biometrico')

const SolicitudesBiometricoModel = require('./models/solicitudes')

const obtenerTodasSolicitudesBiometrico = async (_req, res) => {
    await SolicitudesBiometricoModel.find()
        .then((solicitudes) => {
            if (solicitudes.length > 0) {
                return res.status(200).json({
                    message: 'Biometric requests obtained',
                    solicitudes,
                })
            } else {
                return res.status(204).json({
                    message: 'No biometric requests found',
                })
            }
        })
        .catch((error) => {
            console.log(error)
            return res.status(500).json({
                message: 'Error getting biometric requests',
            })
        })
}

const obtenerSolicitudesPorIndetificacion = async (req, res) => {
    const { numero_indetificacion } = req.params
    await SolicitudesBiometricoModel.find({ numero_indetificacion }).then((solicitudes) => {
        if (solicitudes.length > 0) {
            return res.status(200).json({
                message: 'Biometricos obtenidos',
                solicitudes,
            })
        } else {
            return res.status(204).json({
                message: 'Biometricos no obtenidos'
            })
        }
    })
}
const solicitarCambioBiometrico = async (req, res) => {
    const {
        numero_identificacion,
        nombre_solicitante,
        fecha_identificacion,
        fecha_solicitud,
        hora_identificacion,
        hora_entrada,
        hora_salida,
        estado,
        observacion,
        motivo,
        fecha_aprobacion,
        departamento_identificacion,
    } = req.body
    const Solicitud = new SolicitudesBiometricoModel({
        numero_identificacion,
        nombre_solicitante,
        fecha_identificacion,
        fecha_solicitud,
        hora_identificacion,
        hora_entrada,
        hora_salida,
        estado,
        observacion,
        motivo,
        fecha_aprobacion,
        departamento_identificacion,
        nuevo_registro: false,

    })
    await Solicitud.save()
        .then(() => {
            return res.status(200).json({
                message: 'Biometrico Guardado',
                Solicitud: Solicitud,
            })
        })
        .catch((err) => {
            console.log(err)
            return restart.status(500).json({
                message: 'Error al guardar el biomentrico'
            })
        })
}
const solicitarRegistroBiometrico = async (req, res) => {
    const {
        numero_identificacion,
        nombre_solicitante,
        fecha_solicitud,
        hora_entrada,
        hora_salida,
        estado,
        observacion,
        motivo,
        fecha_aprobacion,
        departamento_identificacion,
    } = req.body

    const Solicitud = new SolicitudesBiometricoModel({
        numero_identificacion,
        nombre_solicitante,
        fecha_identificacion: mongoose.Types.ObjectId(),
        fecha_solicitud,
        hora_identificacion: mongoose.Types.ObjectId(),
        hora_entrada,
        hora_salida,
        estado,
        observacion,
        motivo,
        fecha_aprobacion,
        departamento_identificacion,
        nuevo_registro: true,
    })
    await Solicitud.save()
        .then(() => {
            return res.status(200).json({
                message: 'Biometrico Guardado',
                solicitud: Solicitud,
            })
        })
        .catch((err) => {
            console.log(err)
            return res.status(500).json({
                message: 'Error al guardar el biometrico',
            })
        })

}

const modificarFechaBiometrico = async (
    fecha_identificacion,
    hora_identificacion,
    hora_entrada,
    hora_salida,
) => {
    const query = {
        marcaciones: {
            $elemMatch: {
                _id: fecha_identificacion,
                horas: {
                    $elemMatch: {
                        _id: hora_identificacion,
                    },
                },
            },
        },
    }
    await BiometricoModel.findOne(query)
        .then(async (biometrico) => {
            biometrico.marcaciones.map((marcacion) => {
                if (JSON.stringify(marcacion._id) === JSON.stringify(fecha_identificacion)) {
                    marcacion.horas.map((hora) => {
                        if (JSON.stringify(hora._id) === JSON.stringify(hora_identificacion)) {
                            hora.entrada = hora_entrada
                            hora.salida = hora_salida
                            return hora
                        }
                        return false
                    })
                }
                return false
            })
            await biometrico.save()
            return true
        })
        .catch((err) => {
            console.log(err)
        })
}

const agregarMarcacionPorIdentificacionYFecha = async (
    identificacion,
    fecha,
    hora_entrada,
    hora_salida,
) => {
    await BiometricoModel.findOne({
        numero_identificacion: identificacion,
    }).then((biometrico) => {
        if (biometrico) {
            const marcacion = biometrico.marcaciones.find((marcacion) => {
                if (marcacion.fecha === fecha) {
                    return marcacion
                }
                return false
            })
            if (marcacion) {

                marcacion.horas.push({
                    _id: new mongoose.Types.ObjectId(),
                    entrada: hora_entrada,
                    salida: hora_salida,
                })

                biometrico.markModified('marcaciones')
                biometrico
                    .save()
                    .then((biometrico) => {
                        return biometrico
                    })
                    .catch((error) => {
                        console.log(error)
                        return false
                    })
            } else {

                biometrico.marcaciones.push({
                    _id: mongoose.Types.ObjectId(),
                    fecha,
                    horas: [
                        {
                            _id: mongoose.Types.ObjectId(),
                            entrada: hora_entrada,
                            salida: hora_salida,
                        },
                    ],
                })

                biometrico.markModified('marcaciones')
                biometrico
                    .save()
                    .then((biometrico) => {
                        return biometrico
                    })
                    .catch((error) => {
                        console.log(error)
                        return false
                    })
            }
        } else {
            return false
        }
    })
}

const validarSolicitudBiometrico = async (req, res) => {
    const { id } = req.params
    const { estado, observacion, nuevo_registro } = req.body
    await SolicitudesBiometricoModel.findByIdAndUpdate(
        id,
        { estado, fecha_aprobacion: new Date().toISOString().split('T')[0], observacion },
        { new: true },
    )
        .then(async (solicitud) => {
            if (estado === true) {
                if (!nuevo_registro) {

                    await modificarFechaBiometrico(
                        solicitud.fecha_identificacion,
                        solicitud.hora_identificacion,
                        solicitud.hora_entrada,
                        solicitud.hora_salida,
                    )
                        .then(() => {
                            return res.status(200).json({
                                message: 'Biomentrico actualizado',
                            })
                        })
                        .catch((err) => {
                            console.log(err)
                            return res.status(500).json({
                                message: 'Error al actualizar el biometrico',
                            })
                        })
                } else {
                    await agregarMarcacionPorIdentificacionYFecha(
                        solicitud.numero_identificacion,
                        solicitud.fecha_solicitud,
                        solicitud.hora_entrada,
                        solicitud.hora_salida,
                    )
                        .then(() => {
                            return res.status(200).json({
                                message: 'Biometrico actualizado',
                            })
                        })
                        .catch((err) => {
                            console.log(err)
                            return res.status(500).json({
                                message: 'Error al actualizar',
                            })
                        })
                }
            } else {
                return res.status(200).json({
                    message: 'Biometrico actualizado',
                })
            }
        })
        .catch((err) => {
            console.log(err)
            return res.status(500).json({
                message: 'Erro al actualizar',
            })
        })
}

const eliminarSolititud = async (req, res) => {
    const { id } = req.params
    await SolicitudesBiometricoModel.findByIdAndDelete(id)
        .then((solicitudes) => {
            if (solicitudes === null) {
                return res.status(404).json({
                    message: 'Biometrico borrado ',
                    status: 'error',
                })
            }
        })
        .catch((err) => {
            console.log(err)
            return res.status(500).json({
                message: 'Error al borrar ',
            })
        })
}

module.exports = {
    solicitarCambioBiometrico,
    solicitarRegistroBiometrico,
    obtenerTodasSolicitudesBiometrico,
    obtenerSolicitudesPorIndetificacion,
    validarSolicitudBiometrico,
    eliminarSolititud
}