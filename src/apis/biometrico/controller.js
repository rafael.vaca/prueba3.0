/* eslint-disable prettier/prettier */
require('dotenv').config()

const BiometricoModel = require('./models/biometrico')

const obtenerMarcacionesPorAnio = async (req, res) => {
  const { identificacion, anio } = req.body
  await BiometricoModel.findOne({
    numero_identificacion: identificacion,
    'marcaciones.fecha': { $regex: anio },
  })
    .then((markings) => {
      if (markings) {
        markings.marcaciones = markings.marcaciones.filter((marcacion) => {
          if (marcacion.fecha.startsWith(anio)) {
            return marcacion
          }
          return false
        })
        return res.status(200).json(markings)
      } else {
        return res.status(404).json({
          status: 'error',
          message: 'Markings not found',
        })
      }
    })
    .catch((error) => {
      console.log(error)
      return res.json({
        status: 'error',
        message: 'Markings not found',
      })
    })
}


module.exports = {
  obtenerMarcacionesPorAnio,
}
