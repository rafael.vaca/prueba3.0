/* eslint-disable prettier/prettier */
const swagger = {
    definition: {
      openapi: '3.0.0',
      info: {
        title: 'Prueba 3.0 Backend',
        version: '1.0.0',
      },
      servers: [
        {
          url: 'http://localhost:5000',
        },
      ],
    },
    apis: [
      // './src/apis/products/*.js',
      './src/apis/biometrico/*.js',
      './src/apis/biometrico/solicitudes/*.js',
      './src/apis/biometrico/dias-festivos/*.js',
    ],
  }
  
  module.exports = swagger
  